package assign4;

import java.util.Scanner;

public class Bank {
	public static void main(String[] args) {
		System.out.println("---XYZ BANK---");
		Customer c[] = new Customer[3];
		
		c[0] = new Customer("Anupam","anupam","1234",22,"PV1234567","Ranipool","anupam@gmail.com",8172664532L,100,"Savings");
		c[1] = new Customer("Ashwin","ashwin","1234",22,"PV2123456","Gangtok","ashwin@gmail.com",816364532L,100,"Savings");
		c[2] = new Customer("Archana","archana","1234",22,"PV312345","Kolkata","anupam@gmail.com",8192564532L,200,"Current");
		
		while(true) {
		Customer res = Customer.login(c);
		while(res==null) {
			res = Customer.login(c);
		}
		
		Scanner in = new Scanner(System.in);
		int ch;
		String tempStr;
		float tempFloat; 
		int tempInt;
		long tempLong;
		
		do {
			System.out.println("\n1. Deposit");
			System.out.println("2. Withdrawal");
			System.out.println("3. CheckBalance");
			System.out.println("4. Edit profile");
			System.out.println("5. Change Password");
			System.out.println("6. Exit");
			System.out.print("\n Enter your option: ");
			ch = in.nextInt();
			
			switch(ch) {
				case 1:
					System.out.println("Enter amount to deposit: ");
					tempFloat = in.nextFloat();
					res.deposit(tempFloat);
					break;
				case 2:
					System.out.println("Enter amount to withdraw: ");
					tempFloat = in.nextFloat();
					res.withdrawal(tempFloat);
					break;
				case 3:
					System.out.println("Your balance: "+res.checkBalance());
					break;
				case 4:
					res.editProfile();
					break;
				case 5:
					res.changePassword();
					break;
				case 6:
					break;
				default:
					System.out.println("Invalid Input");
			}
			System.out.println("Do you want to continue: ");
			tempStr=in.next();
			if(tempStr.equals("yes") || tempStr.equals("YES"))
				continue;
			else
				break;
		} while(ch!=6);
		
		if(Customer.loginStatus==true) {
			Customer.loginStatus=false;
		}
		else
			System.out.println("Invalid username/password");
		}
	}
}
