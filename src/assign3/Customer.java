package assign3;

import java.util.Scanner;

public class Customer {
	private String name;
	private String username;
	private String password;
	private int age;
	private String ssn;
	private String address;
	private String email;
	private long phone;
	private float balance;
	private static int custCount;
	public static boolean loginStatus;
	//Adding Account class
	private Account acc;
	private String acctype;
	
	public Customer(String name, String username, String password, int age, String ssn, String address, String email,
			long phone, float balance,String acctype) {
		super();
		this.name = name;
		this.username = username;
		this.password = password;
		this.age = age;
		this.ssn = ssn;
		this.address = address;
		this.email = email;
		this.phone = phone;
		this.balance = balance;
		this.acctype = acctype;
		
		if(acctype.equals("Savings"))
			acc = new Saving();
		else
			acc = new Current();
		custCount++;
	}

	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getPhone() {
		return phone;
	}

	public void setPhone(long phone) {
		this.phone = phone;
	}

	public float checkBalance() {
		return balance;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}
	
	public static Customer login(Customer cus[]) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter username: ");
		String uname = in.next();
		
		System.out.println("Enter password: ");
		String pass= in.next();
		
		for(int x=0; x<custCount;x++) {
			if(cus[x].getUsername().equals(uname) && cus[x].getPassword().equals(pass)) {
				loginStatus=true;
				System.out.println("Login Successful");
				return cus[x];
			}
		}
		System.out.println("Invalid Username and Password");
		return null;
	}

	public void deposit(float val) {
		balance += val;
		System.out.println("Successfully deposited Rs"+val+" to account, current balance: "+balance);
	}
	
	public void withdrawal(float val) {
		if(val>balance)
			System.out.println("Insufficiant Balance");
		else {
			balance -= val;
			System.out.println("Successfully withdrawn Rs"+val+" from account, current balance: "+balance);
		}
	}
	
	public void editProfile() {
		int ch;
			System.out.println("1. Edit name");
			System.out.println("2. Edit email");
			System.out.println("3. Edit password");
			System.out.println("4. Edit address");
			System.out.println("5. Edit age");
			System.out.println("6. Edit phone number");
			System.out.println("Any other option to exit");
			System.out.println("\nEnter choice");
			
			Scanner in = new Scanner(System.in);
			ch = in.nextInt();
			String tempStr;
			float tempFloat;
			int tempInt;
			long tempLong;
			
			switch(ch) {
				case 1:
						System.out.println("Enter new name: ");

						tempStr = in.next();
						setName(tempStr);
						System.out.println("Name updated successfully ");
						break;
						
				case 2:
					System.out.println("Enter new email: ");
					tempStr = in.next();
					setEmail(tempStr);
					System.out.println("Email updated successfully ");
					break;
					
				case 3:
					changePassword();
					break;
					
				case 4:
					System.out.println("Enter new address: ");
					tempStr = in.next();
					setAddress(tempStr);
					System.out.println("Address updated successfully ");
					break;
				
				case 5:
					System.out.println("Enter new age: ");
					tempInt = in.nextInt();
					setAge(tempInt);
					System.out.println("Age updated successfully ");
					break;
					
				case 6:
					System.out.println("Enter new phone number: ");
					tempLong = in.nextLong();
					setPhone(tempLong);
					System.out.println("Phone number updated successfully ");
					break;
				default:
					break;
			}
			
	}
	public void changePassword() {
		Scanner in = new Scanner(System.in);
		String tempStr;
		System.out.println("Enter old password: ");
		tempStr = in.next();
		if(!password.equals(tempStr))
			System.out.println("Incorrect password ");
		else {
			System.out.println("Enter new password: ");
			tempStr = in.next();
			setPassword(tempStr);
		System.out.println("Password changed successfully ");
	}
	}
	
}
