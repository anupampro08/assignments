package assign3;

public class Current extends Account{


	private int noOfTransactionAllowed;
	
	public Current() {
		minimumBalance = 500;
		noOfTransactionAllowed=100;
	}
	
	public void display() {
		super.display();
		System.out.println("Number of Transaction allowed: "+noOfTransactionAllowed);
	}
	
	
}
