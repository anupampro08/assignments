package assign4;

public class Current extends Account{


	private int noOfTransactionAllowed;
	
	public Current() {
		setMinimumBalance(500);
		noOfTransactionAllowed=100;
	}
	
	public void display() {
		System.out.println("Account id: "+getAccountId());
		System.out.println("Description: "+getDescription());
		System.out.println("Minimum Balance "+getMinimumBalance());

		System.out.println("Number of Transaction allowed: "+noOfTransactionAllowed);
	}
	
	
}
