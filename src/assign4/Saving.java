package assign4;

public class Saving extends Account{
	
	private int noOfTransactionAllowed;
	
	public Saving() {
		setMinimumBalance(0);
		noOfTransactionAllowed=10;
	}
	
	public void display() {
		System.out.println("Account id: "+getAccountId());
		System.out.println("Description: "+getDescription());
		System.out.println("Minimum Balance "+getMinimumBalance());
		System.out.println("Number of Transaction allowed: "+noOfTransactionAllowed);
	}
	
	
	
}
