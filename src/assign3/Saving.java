package assign3;

public class Saving extends Account{
	
	private int noOfTransactionAllowed;
	
	public Saving() {
		minimumBalance = 0;
		noOfTransactionAllowed=10;
	}
	
	public void display() {
		super.display();
		System.out.println("Number of Transaction allowed: "+noOfTransactionAllowed);
	}
	
	
	
}
