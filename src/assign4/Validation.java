package assign4;

public interface Validation {
	    public boolean validateSSN(String ssn);
	    public boolean validateAge(int age);
}
