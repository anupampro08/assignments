package assign3;

public class Account {
	private int accountId;
	private String description;
	protected int minimumBalance;
	
	public void display() {
		System.out.println("Account id: "+accountId);
		System.out.println("Description: "+description);
		System.out.println("Minimum Balance "+minimumBalance);
	}

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getMinimumBalance() {
		return minimumBalance;
	}

	public void setMinimumBalance(int minimumBalance) {
		this.minimumBalance = minimumBalance;
	}
}
